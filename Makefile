GOPATH:=$(GOPATH):$(shell pwd)
export GOBIN=$(shell pwd)/bin
.PHONY = pre-build

all: bin/poirot_receiver

test_client: bin/poirot_test_client

bin/%: %.go src/poirot/*.go src/poirot/elasticsearch-template.json.go
	@$(MAKE) pre-build
	go build -o $@ $<

pre-build:
	@go get -v -d poirot
	@go get -v github.com/jteeuwen/go-bindata

src/poirot/elasticsearch-template.json.go: elasticsearch-template.json
	@$(MAKE) pre-build
	bin/go-bindata -out=$@ -pkg="poirot" $<
