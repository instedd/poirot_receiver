package main

import (
	"bufio"
	"github.com/alecthomas/gozmq"
	"log"
	"os"
)

func main() {
	context, _ := gozmq.NewContext()
	socket, _ := context.NewSocket(gozmq.PUB)
	if err := socket.Connect("tcp://localhost:2120"); err != nil {
		log.Fatal(err)
	}

	reader := bufio.NewReader(os.Stdin)

	for {
		line, _, _ := reader.ReadLine()

		if err := socket.Send(line, 0); err != nil {
			log.Fatal(err)
		}
	}
}
