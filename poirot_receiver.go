package main

import (
	"flag"
	"fmt"
	"log"
	. "poirot"
)

func main() {
	var (
		port         = flag.Int("port", 2120, "Listen port")
		index_prefix = flag.String("index_prefix", "poirot", "ElasticSearch index prefix")
		debug        = flag.Bool("debug", false, "Enable debugging output of received messages")
	)
	flag.Parse()

	input, err := Listen(*port)
	if err != nil {
		log.Fatal(err)
	}

	indexer := StartIndexer(*index_prefix)

	input.Debug = *debug
	fmt.Println("Ready")

	for {
		indexer.Index(input.Receive())
	}
}
