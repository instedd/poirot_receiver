package poirot

import (
	"encoding/json"
	"log"
	"time"
)

type Event struct {
	Type string                 `json:"type"`
	Id   string                 `json:"id,omitempty"`
	Body map[string]interface{} `json:"body"`
}

func ParseEvent(data []byte) (*Event, error) {
	var event Event
	err := json.Unmarshal(data, &event)
	return &event, err
}

func (event *Event) TimeStamp() time.Time {
	if ts_text, ok := event.Body["@timestamp"].(string); ok {
		ts, err := time.Parse(time.RFC3339, ts_text)
		if err != nil {
			log.Printf("Error parsing timestamp: %s\n", err)
		} else {
			return ts
		}
	}

	return time.Now()
}
