package poirot

import (
	"fmt"
	"github.com/alecthomas/gozmq"
	"log"
)

type Input struct {
	context *gozmq.Context
	socket  *gozmq.Socket
	Debug   bool
}

func Listen(port int) (*Input, error) {
	context, _ := gozmq.NewContext()
	socket, _ := context.NewSocket(gozmq.SUB)
	socket.SetSockOptString(gozmq.SUBSCRIBE, "")

	err := socket.Bind(fmt.Sprintf("tcp://*:%d", port))
	if err != nil {
		return nil, err
	}

	return &Input{context, socket, false}, nil
}

func (input *Input) Receive() *Event {
	for {
		data, err := input.socket.Recv(0)
		if err != nil {
			log.Fatal(err)
		}

		if input.Debug {
			fmt.Println(string(data[:]))
		}

		if event, err := ParseEvent(data); err != nil {
			log.Println("Could not parse message:", err)
		} else {
			return event
		}
	}
}
