package poirot

import (
	"bytes"
	"github.com/mattbaird/elastigo/api"
	"github.com/mattbaird/elastigo/core"
	"log"
	"text/template"
)

type Indexer struct {
	es     *core.BulkIndexer
	Prefix string
}

func StartIndexer(prefix string) Indexer {
	es := core.NewBulkIndexer(10)
	done := make(chan bool)
	es.Run(done)

	indexer := Indexer{es, prefix}

	// Install index template
	template_json := string(elasticsearch_template_json())
	t := template.Must(template.New("es_template").Parse(template_json))
	buffer := bytes.NewBuffer([]byte{})
	if err := t.Execute(buffer, indexer); err != nil {
		log.Fatal(err)
	}
	if _, err := api.DoCommand("PUT", "/_template/poirot_template", buffer); err != nil {
		log.Fatal(err)
	}

	return indexer
}

func (event *Event) targetIndex(prefix string) string {
	timestamp := event.TimeStamp()
	return timestamp.Format(prefix + "-2006.01.02")
}

func (indexer *Indexer) Index(event *Event) {
	index := event.targetIndex(indexer.Prefix)
	var err error

	switch event.Type {
	case "begin_activity":
		err = indexer.es.Index(index, "activity", event.Id, "", nil, event.Body)
	case "end_activity":
		err = indexer.es.Update(index, "activity", event.Id, "", nil, map[string]interface{}{"doc": event.Body})
	case "logentry":
		err = indexer.es.Index(index, "logentry", "", "", nil, event.Body)
	default:
		log.Printf("Unexpected event type: %s\n", event.Type)
		return
	}

	if err != nil {
		log.Printf("Error indexing entry: %s\n", err)
	}
}
